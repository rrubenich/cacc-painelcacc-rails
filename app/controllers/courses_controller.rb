class CoursesController < ApplicationController
	
	def index
		@courses = Course.all
	end

	def show
		@course = Course.find(params[:id])
	end

	def new
		@course = Course.new
	end

	def create  
		@course = Course.new(course_params)

		if @course.save
			redirect_to @course
		else
			render 'new'
		end
	end

	def destroy
		@course = course.find(params[:id])
		@course.destroy

		redirect_to courses_path
	end

	def edit
		@course = Course.find(params[:id])
	end

	def update
		@course = Course.find(params[:id])

		if @course.update(course_params)
			redirect_to courses_path
		else
			render 'edit'
		end
	end

	# def index
	#   @courses = Course.all
	#   if params[:search]
	#     @posts = Course.search(params[:search]).order("created_at DESC")
	#   else
	#     @posts = Course.all.order('created_at DESC')
	#   end
	# end

	private
	
	def course_params	
		@course = params.require(:course).permit(:name, :nick, :email)
	end
end
