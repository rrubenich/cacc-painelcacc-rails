class TeacherCoursesController < ApplicationController
  def create
    @teacher = Teacher.find(params[:teacher_id])
    @teacher_course = @teacher.teacher_course.create(teacher_course_params)
    redirect_to teachers_path(@teacher)
  end
 
  private
    def teacher_course_params
      params.require(:teacher_course).permit(:course_id, :course_type_id, :year)
    end
end
