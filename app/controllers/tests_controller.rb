class TestsController < ApplicationController
	
	def index
		@tests = Test.all
	end

	def show
		@test = Test.find(params[:id])
	end

	def new
		@test = Test.new
	end

	def create  
		@test = Test.new(test_params)

		if @test.save
			redirect_to @test
		else
			render 'new'
		end
	end

	def destroy
		@test = test.find(params[:id])
		@test.destroy

		redirect_to tests_path
	end

	def edit
		@test = Test.find(params[:id])
	end

	def update
		@test = Test.find(params[:id])

		if @test.update(test_params)
			redirect_to tests_path
		else
			render 'edit'
		end
	end

	private
	
	def test_params	
		@user = params.require(:test).permit(:year, :teacher_id, :course_id, :user_id, :number, :test, :content)
	end

end
