class TeacherCourse < ActiveRecord::Base
  belongs_to :teacher
  belongs_to :course
  belongs_to :course_type
end
