class Test < ActiveRecord::Base
  belongs_to :teacher
  belongs_to :course
  belongs_to :user

  has_attached_file :test
  validates_attachment_content_type :test, :content_type => ['image/jpeg', 'image/png', 'application/pdf']
end
