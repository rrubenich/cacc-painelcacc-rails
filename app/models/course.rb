class Course < ActiveRecord::Base
	has_many :teacher_course


	def self.search(search)
	  where("name LIKE ?", "%#{search}%")
	end
end
