# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150703230907) do

  create_table "course_types", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "courses", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "teacher_courses", force: :cascade do |t|
    t.integer  "year",           limit: 4
    t.integer  "teacher_id",     limit: 4
    t.integer  "course_id",      limit: 4
    t.integer  "course_type_id", limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "teacher_courses", ["course_id"], name: "index_teacher_courses_on_course_id", using: :btree
  add_index "teacher_courses", ["course_type_id"], name: "index_teacher_courses_on_course_type_id", using: :btree
  add_index "teacher_courses", ["teacher_id"], name: "index_teacher_courses_on_teacher_id", using: :btree

  create_table "teachers", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "email",      limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "tests", force: :cascade do |t|
    t.integer  "teacher_id",        limit: 4
    t.integer  "course_id",         limit: 4
    t.integer  "user_id",           limit: 4
    t.integer  "year",              limit: 4
    t.integer  "number",            limit: 4
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.string   "test_file_name",    limit: 255
    t.string   "test_content_type", limit: 255
    t.integer  "test_file_size",    limit: 4
    t.datetime "test_updated_at"
    t.text     "content",           limit: 65535
  end

  add_index "tests", ["course_id"], name: "index_tests_on_course_id", using: :btree
  add_index "tests", ["teacher_id"], name: "index_tests_on_teacher_id", using: :btree
  add_index "tests", ["user_id"], name: "index_tests_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "name",                limit: 255
    t.string   "email",               limit: 255
    t.string   "nick",                limit: 255
    t.string   "password",            limit: 255
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.string   "avatar_file_name",    limit: 255
    t.string   "avatar_content_type", limit: 255
    t.integer  "avatar_file_size",    limit: 4
    t.datetime "avatar_updated_at"
  end

  add_foreign_key "teacher_courses", "course_types"
  add_foreign_key "teacher_courses", "courses"
  add_foreign_key "teacher_courses", "teachers"
  add_foreign_key "tests", "courses"
  add_foreign_key "tests", "teachers"
  add_foreign_key "tests", "users"
end
