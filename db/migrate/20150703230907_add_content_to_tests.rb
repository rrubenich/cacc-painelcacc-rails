class AddContentToTests < ActiveRecord::Migration
  def self.up
    change_table :tests do |t|
      t.text :content
    end
  end
end
