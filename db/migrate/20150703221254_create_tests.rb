class CreateTests < ActiveRecord::Migration
  def change
    create_table :tests do |t|
      t.references :teacher, index: true, foreign_key: true
      t.references :course, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
      t.integer :year
      t.integer :number

      t.timestamps null: false
    end
  end
end
