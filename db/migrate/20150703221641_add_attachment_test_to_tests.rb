class AddAttachmentTestToTests < ActiveRecord::Migration
  def self.up
    change_table :tests do |t|
      t.attachment :test
    end
  end

  def self.down
    remove_attachment :tests, :test
  end
end
