class CreateTeacherCourses < ActiveRecord::Migration
  def change
    create_table :teacher_courses do |t|
      t.integer :year
      t.references :teacher, index: true, foreign_key: true
      t.references :course, index: true, foreign_key: true
      t.references :course_type, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
